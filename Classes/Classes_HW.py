import pytest
import json
import keyword


def json_open(file_name: str) -> dict:
    """
    Функция четения файла JSON
    """
    with open(file_name, "r", encoding="utf-8") as json_data:
        data = json.load(json_data)
        json_data.close()
        return data


class ColorizeMixin(object):
    """
    Класс для реализации функции окрашивания __repr__
    """
    repr_color_code = 30

    def __repr__(self):
        return f'\033[0;', ';49m', '\033[0;39;48m'


class Advert(ColorizeMixin):
    """
    Основной класс
    """
    data = {}

    def __init__(self, data):
        self.repr_color_code = 33
        self.data = data
        self.price = 0
        self.dict_to_class(self.data)

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, new_price):
        if new_price < 0: # проверка цены
            raise ValueError
        else:
            self._price = new_price

    def dict_to_class(self, subdata: dict):
        for obj in subdata:
            if keyword.iskeyword(obj):
                new_obj = obj + '_'
            else:
                new_obj = obj
            if isinstance(subdata[obj], dict):
                setattr(self, new_obj, type(new_obj, (Advert,), subdata[obj]))
                eval(f'self.{new_obj}.dict_to_class(self, subdata[obj])')
            elif obj == 'price':
                self.price = subdata[obj]
            else:
                setattr(self, new_obj, subdata[obj])

    def __repr__(self):
        mixin_str = ColorizeMixin.__repr__(self)
        try:
            return mixin_str[0] +\
                   f'{self.repr_color_code}' + \
                   mixin_str[1] + \
                   f'{self.title} | {self.price} ₽' + \
                   mixin_str[2]
        except AttributeError:
            return 'В файле нет аттрибута title.'


def test_iphone_location_address():
    """
    Тестирование на обращение через точку к аттрибуту address
    """
    iphone_advert = Advert(json_open('Iphone.json'))
    assert iphone_advert.location.address == "город Самара, улица Мориса Тореза, 50"


def test_iphone_wrong_price():
    """
    Тестирование на цену < 0
    """
    with pytest.raises(ValueError):
        Advert(json_open('Iphone_wrong_price.json'))


def test_iphone_no_price():
    """
    Тестирование на отсутствие цены
    """
    iphone_advert = Advert(json_open('Iphone_no_price.json'))
    assert iphone_advert.price == 0


def test_corgi_keyword():
    """
    Тестирование на присутсвие ключевого слова
    """
    corgi_advert = Advert(json_open('Corgi.json'))
    assert corgi_advert.class_ == 'dogs'


def test_corgi_mixin():
    """
    Тестирование изменения цвета __repr__
    """
    corgi_advert = Advert(json_open('Corgi.json'))
    assert f'{corgi_advert}' == '\033[0;33;49mВельш-корги | 1000 ₽\033[0;39;48m'


def test_corgi_notitle():
    """
    Тестирование отсутвия аттрибута title
    """
    corgi_advert = Advert(json_open('Corgi_notitle.json'))
    assert f'{corgi_advert}' == 'В файле нет аттрибута title.'


print(Advert(json_open('Corgi.json')))
