'''
Функциональное программирование

1. Чистые функции
Зависят только от передаваемых параметров и возвращают один и тот же результат

2. Функции высшего порядка
Функции, которые принимают функцию и/или возвращают функцию
'''

#def filter_bool(seq):

def my_filter(pred, seq):
    for x in seq:
        if pred(x):
            yield x

users = ['0', '1', '2', '3']
#print(next(filter(bool,map(int, users))))



us = [int(x) for x in users if int(x)!=0 ]
#print(us)

from functools import partial

def filter(pred, items):
    return (x for x in items if pred(x))

filter_bool = partial(filter, bool)
items = ('', 3, None)
res=filter_bool(items)


print(filter(bool, items))


def add(x,y):
    return x+y


y_add = partial(add, 1)
print(f'y_add result = {y_add(5)}')

from functools import wraps

def mult_3(func):
    @wraps(func)
    def inner(x: int):
        '''Not foo'''
        return func(x) * 3
    return inner

def mult(m):
    def deco(func):
        @wraps(func)
        def inner(*args):
            '''Not foo'''
            return func(*args) * m
        return inner
    return deco
@mult(5)
def foo(y: int, x: int):
    '''Documentation'''
    return x+y

#foo = mult(4, foo)

print(foo(1,3))
#print(foo.__name__, foo.__doc__)

def bar(**kwargs):
    kwargs['b'] = 2
    print(kwargs)

data = {'a':'1'}
bar(**data)
print(data)

#from  deepcopy import deepcopy для копирования всей структуры словаря

def compose (*fns):
    init, *rest = reversed(fns)

    def inner (*args, **kwargs):
        result = init(*args, **kwargs)
        for fn in rest:
            result = fn(result)
        return result
    return inner

mapv = compose(list, map)
print(mapv(str, [1,2]))
#val = map(str, [1,2])


from  functools import reduce
print(reduce(lambda a, x: a+x, [1, 2, 3]))