import pytest
from typing import Iterable


# TODO№1  Написать функцию получения размера генератора
def ilen(iterable: Iterable):
    cnt = 0
    for i in iterable:
        cnt = cnt+1
    return cnt


@pytest.mark.parametrize('test_input,expected', [((x for x in range(10)), 10),
                                                 ({'1': 'a', '2': 'b'}, 2),
                                                 (set('12345'), 5),
                                                 ([1, 2, 3, 4, 5], 5),
                                                 ((1, 2, 3, 4, 5), 5),
                                                 (range(0), 0)])
def test_ilen(test_input: Iterable, expected: int) -> None:
    """
        Тестирование функции ilen на разные типы данных (generator, dict, set, list, tuple).
    """
    assert ilen(test_input) == expected


# TODO№2 Написать функцию flatten, которая из многоуровневого массива сделает одноуровневый
def flatten(iterable: Iterable):
    for obj in iterable:
        if isinstance(obj, Iterable) and not isinstance(obj, str):  # если объект итерируемый и не строка
            for subiterable in flatten(obj):
                yield subiterable  # рекурсивный вызов функции
        else:
            yield obj


@pytest.mark.parametrize('test_input,expected', [([0, [1, [2, 3]]], [0, 1, 2, 3]),
                                                 (['ab', ['cd', ['ef', 'gh']]], ['ab', 'cd', 'ef', 'gh'])])
def test_flatten(test_input: Iterable, expected: Iterable) -> None:
    """
        Тестирование функции flatten
    """
    assert list(flatten(test_input)) == expected


# TODO№3 Написать функцию, которая удалит дубликаты, сохранив порядок
def distinct(iterable: Iterable):
    pass_elemnts = set()
    for element in iterable:
        if element in pass_elemnts:
            continue
        else:
            pass_elemnts.add(element)
            yield element


@pytest.mark.parametrize('test_input,expected', [([1, 2, 0, 1, 3, 0, 2], [1, 2, 0, 3]),
                                                 (['Rio', 'Paris', 'Moscow', 'Rio'], ['Rio', 'Paris', 'Moscow'])])
def test_distinct(test_input: Iterable, expected: Iterable) -> None:
    """
        Тестирование функции distinct.
    """
    assert list(distinct(test_input)) == expected


# TODO№4 Неупорядоченная последовательность из словарей, сгруппировать по ключу, на выходе словарь
def groupby(key, iterable: Iterable):
    values_set = set()
    for element in iterable:
        values_set.add(element[key])
    values_list = list(values_set)
    values_list.sort()
    print(values_list)
    for value in values_list:
        for element in iterable:
            if element[key] != value:
                continue
            yield element


def test_groupby_age() -> None:
    """
        Тестирование функции groupby
    """
    users = [
        {'gender': 'female', 'age': 33},
        {'gender': 'male', 'age': 20},
        {'gender': 'female', 'age': 21},
    ]
    result = [
        {'gender': 'male', 'age': 20},
        {'gender': 'female', 'age': 21},
        {'gender': 'female', 'age': 33},
    ]
    assert list(groupby('age', users)) == result


def test_groupby_gender() -> None:
    """
        Тестирование функции groupby
    """
    users = [
        {'gender': 'female', 'age': 33},
        {'gender': 'male', 'age': 20},
        {'gender': 'female', 'age': 21},
    ]
    result = [
        {'gender': 'female', 'age': 33},
        {'gender': 'female', 'age': 21},
        {'gender': 'male', 'age': 20},
    ]
    assert list(groupby('gender', users)) == result


def test_groupby_keyerror() -> None:
    """
        Тестирование функции groupby на отсутсвующий ключ
    """
    users = [
        {'gender': 'female', 'age': 33},
        {'gender': 'male', 'age': 20},
        {'age': 21},
    ]
    with pytest.raises(KeyError):
        list(groupby('gender', users))


# TODO#5 Написать функцию, которая разобьет последовательность на заданные куски
def chunks(size: int, iterable: Iterable):
    packet = []
    for cnt in range(size):
        packet.append(None)
    cnt = 0
    for obj in iterable:
        packet[cnt] = obj
        if cnt == size-1:
            cnt = 0
            yield tuple(packet)
        else:
            cnt = cnt + 1
    while cnt < size:
        packet[cnt] = None
        cnt = cnt + 1
    yield tuple(packet)


@pytest.mark.parametrize('test_input_size,test_input_value,expected',
                         [(3, [0, 1, 2, 3, 4], [(0, 1, 2), (3, 4, None)]),
                          (4, (x for x in range(5)), [(0, 1, 2, 3), (4, None, None, None)])])
def test_chunks(test_input_size: int, test_input_value: Iterable, expected: Iterable) -> None:
    """
        Тестирование функции chunks
    """
    assert list(chunks(test_input_size, test_input_value)) == expected


# TODO#6 Написать функцию получения первого элемента или None
def first(iterable: Iterable):
    try:
        return next(iterable)
    except TypeError:
        return None
    except StopIteration:
        return None


@pytest.mark.parametrize('test_input,expected', [((x for x in range(10)), 0),
                                                 (range(0), None)])
def test_first(test_input: Iterable, expected) -> None:
    """
        Тестирование функции first
    """
    assert first(test_input) == expected


def test_some_first() -> None:
    """
        Тестирование функции first при отсутствии элементов
    """
    example = (x for x in range(1))
    first(example)
    assert first(example) is None


# TODO№7 Написать функцию получения последнего элемента или None
def last(iterable: Iterable):
    try:
        base_obj = next(iterable)
        for obj in iterable:
            base_obj = obj
    except TypeError:
        return None
    except StopIteration:
        return None
    else:
        return base_obj


@pytest.mark.parametrize('test_input,expected', [((x for x in range(10)), 9),
                                                 (range(0), None),
                                                 (range(5), None)])
def test_last(test_input: Iterable, expected) -> None:
    """
        Тестирование функции chunks
    """
    assert last(test_input) == expected


def test_double_last() -> None:
    """
        Тестирование функции last на двойной вызов
    """
    example = (x for x in range(10))
    last(example)
    assert last(example) is None
