steps = dict()


def ShowInventory(inventory: set):
    if inventory == set():
        print('Карманы Дака пусты, как бутылки виски, стоящие у него на столе.')
    else:
        print('У Дака есть:')
        for item in inventory:
            print(f'-{item}')


def AddInventory(inventory: set, current_step: str):
    if current_step == 'restroom4' or current_step == 'restroom3':
        inventory.add('сигареты')
    elif current_step == 'bedroom2':
        inventory.add('Патроны к пистолету')
    elif current_step == 'crime2':
        inventory.add('Улики с места преступления')
    elif current_step == 'prison3':
        inventory.add('Номер телефона мистера Вулфа')
    if current_step == 'lobby2':
        inventory.add('Пистолет')
        if {'Патроны к пистолету'}.issubset(inventory):
            inventory.add('Пистолет с патронами')


def Choice(max_value: int) -> int:
    while True:
        try:
            choice = int(input())
            if choice < 1 or choice > max_value:
                raise ValueError
        except ValueError:
            print(f'Неверный ввод. Введите число от 1 до {max_value}.')
        else:
            return choice


def Step(steps: dict, st_name: str, inventory: set) -> str:
    print(steps[st_name]['st_description'])
    for idx, action in enumerate(steps[st_name]['st_actions']):
        print(f'{idx+1}. {action}')
    print(f'{idx+2}. Проверить карманы.')
    print(f'Выбери действие: 1-{idx+2}.')
    choice = Choice(idx+2)
    if choice == idx+2:
        ShowInventory(inventory)
        return st_name
    else:
        need_item = set()
        need_item.add(steps[st_name]['st_condition'][choice-1])
        if need_item == {'None'} or need_item.issubset(inventory):
            return steps[st_name]['st_next_hops'][choice-1]
        else:
            print(f'У Дака нет необходимого предмета: {need_item}.')
            return st_name


def AddStep(steps: dict,
            st_name: str,
            st_description: str, st_actions: list, st_next_hops: list, st_condition: list):
    steps.setdefault(st_name,
                     dict(st_description=st_description,
                          st_actions=st_actions,
                          st_next_hops=st_next_hops,
                          st_condition=st_condition))


def Game(steps: dict):
    inventory = set()
    current_step = Step(steps, 'phone1', inventory)
    while current_step is not 'final':
        current_step = Step(steps, current_step, inventory)
        AddInventory(inventory, current_step)


phone1_description = '12 октября 1985.\n' \
                     'Лучший город… Некогда прекрасное место в великом штате, великой страны…\n' \
                     'Сейчас же насквозь коррумпированная дыра, пропитанная сладкоголосой болтовнёй либералов,\n' \
                     'похожая на нору протухшего суслика.\n' \
                     'В Лучшем городе либо выживают с широко закрытым звериным оскалом,\n' \
                     'либо…не выживают…\n\n' \
                     'Ночь Лучшего города… Страшное время страшного места… Если нет звуков чавканья,\n' \
                     'значит какой-то "везунчик" уже переваривается в брюхе соседа по пищевой цепи.\n\n' \
                     'Скромная квартира в Старом районе на окраине Лучшего города не отличалась,\n' \
                     'шиком или комфортом, но в ней были четыре стены и крыша.\n' \
                     'А что ещё может потребоваться частному детективу Даку Джонсону после того,\n' \
                     'как из-за хронического трудоголизма от него ушёл единственный лучик этого мрачного мира –\n' \
                     'его жена Кэрол? В конце концов, как она часто поговаривала: "Лучше дома места нет".\n\n' \
                     'Тишина ночи была разорвана трамвайным звонком старого телефона в кабинете детектива Джонсона.\n\n' \
                     '-Детектив Дак Джонсон слушает, - сонным, но твёрдым голосом ответил главный герой, сняв трубку.\n' \
                     '-Дак, это шериф Смит, - пробормотал телефон. – Обнаружен труп в твоём районе, возможно, тебе будет интересно.\n'
phone1_actions = ['Выслушать шерифа', 'Мне это не нужно']
phone1_next_hops = ['cabinet1', 'phone2']
phone1_condition = ['None', 'None']
AddStep(steps, 'phone1', phone1_description, phone1_actions, phone1_next_hops, phone1_condition)

phone2_description = '-С каких пор полицейская контора о таких делах информирует частных детективов? Меня это не интересует.\n\n' \
                     '-Понимаю, ты уже спёкся, и живёшь в ожидании пенсии. Не стоило мне тебе звонить. Прощай…\n'
phone2_actions = ['Выйти из игры']
phone2_next_hops = ['final']
phone2_condition = ['None']
AddStep(steps, 'phone2', phone2_description, phone2_actions, phone2_next_hops, phone2_condition)

cabinet1_description = '-С каких пор полицейская контора по таким делам обращается к частным детективам?\n' \
                       'Но продолжай, раз разбудил.\n\n' \
                       'Ответ заставил Дака присесть на массивное кожаное кресло,\n' \
                       'стоящее перед резным дубовым столом. Остатки былой роскоши…\n\n' \
                       '-…Скоро буду. – хладно ответил Дак, положил трубку и хотел было закурить,\n' \
                       'на пошарив по карманам, вспомнил,\n' \
                       'что сигареты в ванной и начал собираться на место преступления.\n' \
                       '"Ладно, не до этого… Надо торопиться… Главное не забыть…".\n'
cabinet1_actions = ['Выйти в прихожую', 'Зайти в ванную', 'Зайти в спальню']
cabinet1_next_hops = ['lobby1', 'restroom1', 'bedroom1']
cabinet1_condition = ['None', 'None', 'None']
AddStep(steps, 'cabinet1', cabinet1_description, cabinet1_actions, cabinet1_next_hops, cabinet1_condition)

cabinet2_description = '-У меня больше нет времени шатания по квартире. Пора уходить...\n'
cabinet2_actions = ['Выйти в прихожую']
cabinet2_next_hops = ['lobby1']
cabinet2_condition = ['None']
AddStep(steps, 'cabinet2', cabinet2_description, cabinet2_actions, cabinet2_next_hops, cabinet2_condition)

cabinet3_description = '-У меня больше нет времени на водные процедуры. Пора уходить...\n'
cabinet3_actions = ['Выйти в прихожую']
cabinet3_next_hops = ['lobby1']
cabinet3_condition = ['None']
AddStep(steps, 'cabinet3', cabinet3_description, cabinet3_actions, cabinet3_next_hops, cabinet3_condition)

restroom1_description = 'В ванной было всё как обычно, только разбитое зеркало гнетет Дака.\n\n' \
                        'На бачке унитаза лежит пачка сигарет.\n'
restroom1_actions = ['Вернуться в кабинет', 'Умыться', 'Взять сигареты']
restroom1_next_hops = ['cabinet2', 'restroom2', 'restroom3']
restroom1_condition = ['None', 'None', 'None']
AddStep(steps, 'restroom1', restroom1_description, restroom1_actions, restroom1_next_hops, restroom1_condition)

restroom2_description = 'Что за красавчик в зеркале...\n'
restroom2_actions = ['Вернуться в кабинет', 'Взять сигареты']
restroom2_next_hops = ['cabinet2', 'restroom4']
restroom2_condition = ['None', 'None']
AddStep(steps, 'restroom2', restroom2_description, restroom2_actions, restroom2_next_hops, restroom2_condition)

restroom3_description = '-Стоит взять этих "Солдатиков смерти". Вдруг пригодятся.\n'
restroom3_actions = ['Вернуться в кабинет', 'Умыться']
restroom3_next_hops = ['cabinet2', 'restroom4']
restroom3_condition = ['None', 'None']
AddStep(steps, 'restroom3', restroom3_description, restroom3_actions, restroom3_next_hops, restroom3_condition)

restroom4_description = 'Больше в ванной делать нечего...'
restroom4_actions = ['Вернуться в кабинет']
restroom4_next_hops = ['cabinet2', 'restroom4']
restroom4_condition = ['None', 'None']
AddStep(steps, 'restroom4', restroom4_description, restroom4_actions, restroom4_next_hops, restroom4_condition)

bedroom1_description = 'На тумбочке лежит заряженный магазин для пистолета.\n\n' \
                       '-Стоит взять этих "Солдатиков смерти". Вдруг пригодятся.\n'
bedroom1_actions = ['Вернуться в кабинет', 'Взять патроны для пистолета']
bedroom1_next_hops = ['cabinet3', 'bedroom2']
bedroom1_condition = ['None', 'None']
AddStep(steps, 'bedroom1', bedroom1_description, bedroom1_actions, bedroom1_next_hops, bedroom1_condition)

bedroom2_description = '-С ним спокойнее, пора бежать.\n'
bedroom2_actions = ['Вернуться в кабинет']
bedroom2_next_hops = ['cabinet3']
bedroom2_condition = ['None']
AddStep(steps, 'bedroom2', bedroom2_description, bedroom2_actions, bedroom2_next_hops, bedroom2_condition)

lobby1_description = 'В прихожей на тумбочке лежит пистолет.\n'
lobby1_actions = ['Поехать на место преступления', 'Взять пистолет']
lobby1_next_hops = ['crime1', 'lobby2']
lobby1_condition = ['None', 'None']
AddStep(steps, 'lobby1', lobby1_description, lobby1_actions, lobby1_next_hops, lobby1_condition)

lobby2_description = 'С ним будет спокойнее, по коням.\n.'
lobby2_actions = ['Поехать на место преступления']
lobby2_next_hops = ['crime1']
lobby2_condition = ['None']
AddStep(steps, 'lobby2', lobby2_description, lobby2_actions, lobby2_next_hops, lobby2_condition)

crime1_description = 'Выход на улицу был ознаменован глубоким вдохом детектива,\n' \
                     'который вызвал секундное головокружение. Сев в красный Кадиллак,\n' \
                     'Дак ударил по педали газа и уже через 5 минут был на месте преступления.\n' \
                     'Заграждения из полицейских машин и пёстрая жёлтая лента обвивали \n' \
                     'свежий след мела на ночном асфальте.\n' \
                     'Ещё полчаса назад на этом месте лежал бывший напарник главного героя Том.\n\n' \
                     'Не сказать, что на протяжении всей их совместной деятельности у них было всё гладко,\n' \
                     'но они были хорошими напарниками. Не в том смысле, что держались за руку при лунном свете,\n' \
                     'но не раз выдёргивали друг друга из всяких передряг.\n\n' \
                     '-Даки. До последнего не верил, что ты приедешь. - «поздоровался» шериф.\n\n' \
                     '-Шериф.\n\n' \
                     '-Ловить, на самом деле, тут особо нечего. Может двинем в полицейский участок?\n'
crime1_actions = ['Поехать в полицейский участок.', 'Пока рано, нужно осмотреться.']
crime1_next_hops = ['police1', 'crime2']
crime1_condition = ['None', 'None']
AddStep(steps, 'crime1', crime1_description, crime1_actions, crime1_next_hops, crime1_condition)

crime2_description = 'Задушив жгучее желание проявить слабость и покинуть это мерзкое место ответил:\n\n' \
                     '-Ты меня на вечернюю прогулку пригласил. Дай мне время осмотреться.\n\n' \
                     'Шериф мрачно кивнул.\n\n' \
                     'Поиски увенчались успехом, детектив заприметил то, что от других было скрыто у всех на виду.\n'
crime2_actions = ['Поехать в полицейский участок']
crime2_next_hops = ['police1']
crime2_condition = ['None']
AddStep(steps, 'crime2', crime2_description, crime2_actions, crime2_next_hops, crime2_condition)

police1_description = 'Некогда место наибольшего времяпрепровождения детектива.\n' \
                      'В профессиональном мозгу всплыла дилемма: освежить данные о бывшем напарнике\n' \
                      'или погрузиться в анализ улик.\n'
police1_actions = ['Посмотреть информацию о Томе', 'Изучить улики с места преступления']
police1_next_hops = ['police2', 'police3']
police1_condition = ['None', 'Улики с места преступления']
AddStep(steps, 'police1', police1_description, police1_actions, police1_next_hops, police1_condition)

police2_description = 'Усталыми глазами детектив водил по досье, которое видел миллионы раз.\n' \
                      'Личные данные, собственность, счета, привычки, данные о родных.\n' \
                      'Именно тут и была наводка на возможный источник информации: его брат Тим.\n' \
                      'Бывший наркоман. Помещён в клинику Голдгейт.\n' \
                      'Из-за наблюдаемой ремиссии выпущен пару недель назад. А не стоит ли задать ему пару вопросов?\n'
police2_actions = ['Отправиться к брату жертвы', 'Изучить улики с места преступления']
police2_next_hops = ['brother1', 'police4']
police2_condition = ['None', 'Улики с места преступления']
AddStep(steps, 'police2', police2_description, police2_actions, police2_next_hops, police2_condition)

police3_description = 'Вглядываясь в фотографию протектора шин,\n' \
                      'сделанную на месте преступления в мыслях детектива всплывали воспоминания\n' \
                      'об опыте “взаимодействия” с одним байкерским клубом. Одним из знаков его отличия,\n' \
                      'чуть ли не на ровне с цветами было использование именно этой марки резины,\n' \
                      'именно с этим рисунком протектора. Нечто вроде фирменного клейма на люксовой безделушке.\n\n' \
                      'Проблема в том, что президент этого клуба Питер Хонда уже полгода отбывает\n' \
                      'один из ста пожизненных сроков в Даклэйке,\n' \
                      'тюрьме особого назначения, но свидания с ним не запрещены…Почему бы и нет?\n'
police3_actions = ['Посмотреть информацию о жертве', 'Отправиться в тюрьму']
police3_next_hops = ['police4', 'prison1']
police3_condition = ['None', 'None']
AddStep(steps, 'police3', police3_description, police3_actions, police3_next_hops, police3_condition)

police4_description = 'Проанализировав досье и улики, у Дака появились две зацепки:\n\n' \
                      '-Навестить брата-наркомана Тома - Тима, выпущенного из лечебницы Голдгейт не так давно.\n' \
                      '-Съездить в тюрьму к президенту мотоклуба  Питеру Хонде,\n' \
                      'который наверняка знает убийцу.\n\n' \
                      'Что же выбрать?'
police4_actions = ['Отправиться к брату жертвы', 'Отправиться в тюрьму']
police4_next_hops = ['brother1', 'prison1']
police4_condition = ['None', 'None']
AddStep(steps, 'police4', police4_description, police4_actions, police4_next_hops, police4_condition)

brother1_description = 'Тишина. Резкий стук в облезлую деревянную дверь,\n' \
                       'находящуюся в середине коридора 12-го этажа.\n' \
                       'Тишина. Неторопливые шаги. Приближающийся запах тяжёлого диалога.\n\n' \
                       '-Кто? -раздался голос из-за двери.\n\n' \
                       '-Тим? Это Дак Джонсон.\n\n' \
                       '-Среди ночи, что тебя сюда завело?\n\n' \
                       'Может откроешь? Речь пойдёт о твоём брате.\n\n' \
                       'Через секунду дверь отворилась и взору предстал изрядно потрёпанный силуэт.\n' \
                       'Не сказать, что после реабилитации прожжённые наркоманы выглядят\n' \
                       'как лондонские денди, но Тим, так особенно.\n\n' \
                       '-Он меня мало интересует, если что с ним произошло, то это было заслуженно.\n\n' \
                       'После этих слов глаза Дака заволокла красная вуаль единственное, что он не мог решить:\n' \
                       'использовать ли пистолет или попытаться выбить все что можно из этого нарколыги руками.\n'
brother1_actions = ['Выбить из него правду', 'Пригрозить пистолетом']
brother1_next_hops = ['brother2', 'brother3']
brother1_condition = ['None', 'Пистолет']
AddStep(steps, 'brother1', brother1_description, brother1_actions, brother1_next_hops, brother1_condition)

brother2_description = 'В глазах Тима сверкнула вспышка. Хруст переносицы. Металлический привкус крови во рту.\n' \
                       'Падение. Резкий подъём за ворот и удар. Ещё удар… И ещё… Тишина…\n\n' \
                       '-Теперь ты настроился на разговор бездушная ты свинья. Сообщаю, твой брат убит сегодня.\n' \
                       'Либо ты расскажешь МНЕ что-то любознательное, либо санитарам в Голдгейте.\n\n' \
                       'Слегка опомнившись, хлюпая носом, Тим выдавил: Фокс…\n' \
                       'Недавно я слышал от Тома это имя в его телефонном разговоре,\n' \
                       'когда приходил к нему попросить денег. Он послал меня.\n\n' \
                       'Это всё, что я знаю, -поднимаясь с колен произнёс Тим,\n' \
                       'но повторюсь- всё что произошло-заслуженно и я чертовски рад этому.\n'
brother2_actions = ['Выбросить его из окна и поехать за убийцей', 'Оставить его и уйти']
brother2_next_hops = ['arrest1', 'brother4']
brother2_condition = ['None', 'None']
AddStep(steps, 'brother2', brother2_description, brother2_actions, brother2_next_hops, brother2_condition)

brother3_description = 'Щелчок затвора сделал свое дело. Как говорится, добрым словом и пистолетом можно добиться\n' \
                       'куда большего, чем одним лишь добрым словом. Хорошая фраза, жаль не помню кто сказал,\n' \
                       'может Рузвельт, наверняка, кто- то из его окружения.\n\n' \
                       'У меня есть только имя «Фокс», но мне этого достаточно. А счастливчику Тиму\n' \
                       'достанется остаток его жизни, который он филигранно сможет спустить в унитаз.\n.'
brother3_actions = ['Поехать за убийцей']
brother3_next_hops = ['arrest1']
brother3_condition = ['None']
AddStep(steps, 'brother3', brother3_description, brother3_actions, brother3_next_hops, brother3_condition)

brother4_description = 'Детектив хладнокровно отнёсся к этим словам и направился к выходу.\n' \
                       'Чего слушать нарика, обженного на жизнь. В дверях Дак развернулся. Выстрел.\n' \
                       'На фоне окна стоит Тим. В руках у него Магнум с дымящимся стволом.\n' \
                       'Ещё один выстрел. Мрак… Тишина…\n'
brother4_actions = ['Выйти из игры']
brother4_next_hops = ['final']
brother4_condition = ['None']
AddStep(steps, 'brother4', brother4_description, brother4_actions, brother4_next_hops, brother4_condition)

prison1_description = 'Тюрьма особого назначения Даклэйке. Готического вида постройка…\n' \
                      'Неприступная крепость. Гостиница при казино Фемиды.\n\n' \
                      'Маньяки, убийцы и прочие уважаемые люди современного общества, можно сказать,\n' \
                      'самые его “сливки”, все, как один, невиновны.\n' \
                      'Скольким из них Дак забронировал номер в дни ушедшие…\n\n' \
                      'Обозначив цель визита, детектив был проведён в комнату для свиданий-\n' \
                      'длинный коридор вдоль разделённый столами со стеклянными перегородками.\n' \
                      'Дак сел за стол, поднял голову, глубоко вдохнул и приготовился к разговору.\n' \
                      'В течение пяти минут по ту сторону стола появился худой и прямой,\n' \
                      'как стрела стан собеседника в оранжевой робе заключенного.\n\n' \
                      '-Офицер Дак-уверенно произнёс Питер Хонда.\n\n' \
                      '-Бывший. – ответил детектив.\n\n' \
                      '-Каким ветром?\n\n' \
                      '-Вольным. давненько такой не ощущал? хотел задать тебе пару вопросов.\n\n' \
                      '-Давненько… Быть может ты напомнишь. Не найдётся закурить?\n'
prison1_actions = ['Шантажом вынудить сказать, кто убийца', 'Проявить "добродушие" и выяснить, кто убийца']
prison1_next_hops = ['prison2', 'prison3']
prison1_condition = ['None', 'Сигареты']
AddStep(steps, 'prison1', prison1_description, prison1_actions, prison1_next_hops, prison1_condition)

prison2_description = '-Обойдёшься. Ты думаешь, что я приехал сюда привезти тебе курево?\n' \
                      'Этой ночью был убит Том Томас, у тебя на улице ушей, как конечностей у сороконожки.\n' \
                      'Ничего мне рассказать не хочешь?\n\n' \
                      '-А если я откажусь?\n\n' \
                      '-Это твоё право. Но жить в этом прелестном месте будет менее приятно, а главное,\n' \
                      'менее продолжительно. Нет, лично я с тобой ничего не сделаю, но ты поймёшь.\n' \
                      'Ты настроился на сотрудничество?\n\n' \
                      '-Да, бывший офицер. У меня есть информация, что к этому причастен Фокс.\n' \
                      'Добавить мне нечего. Встреча закончена.\n\n' \
                      'После этих слов, Питер встал и, в сопровождении конвоя был уведён в недра тюрьмы.\n'
prison2_actions = ['Поехать на задержание']
prison2_next_hops = ['arrest1']
prison2_condition = ['None']
AddStep(steps, 'prison2', prison2_description, prison2_actions, prison2_next_hops, prison2_condition)

prison3_description = '-Да, без проблем. Ты же эти любишь? - Спросил детектив демонстрируя белоснежную пачку,\n' \
                      'середина лицевой стороны которой была увенчана золотым оттиском короны.\n\n' \
                      '-О, да, «Грейт Краун», кто-то тебе меня сдал.\n\n' \
                      'Дак просунул почти полую пачку сигарет в щель между стеклом и столом.\n\n' \
                      '-Ты не жадный, я знаю по какому поводу ты пришёл, уже в курсе смерти Тома.\n' \
                      'Мне шепнули, что за этим стоит Фокс.\n' \
                      'Благодаря решётке, руки у меня сейчас коротки, но желание отомстить за Тома очень велико,\n' \
                      'вероятно, как и у тебя.\n\n' \
                      'Питер взял пачку, а Дак почувствовал сухой бумажный шелест,\n' \
                      'после чего обнаружил у себя сложенный пополам клочок бумаги.\n\n' \
                      '-Позвони ему. Мистер Вульф тебе поможет.\n' \
                      'Это самое малое что я могу сделать для тебя в память о Томе.\n' \
                      'Рад был встретиться, детектив. Ответь себе, а бывают ли офицеры бывшими? Подумай…\n\n' \
                      'Этими словами встреча была окончена, а Питер растворен в лабиринте коридоров Даклэйка.\n'
prison3_actions = ['Поехать на задержание']
prison3_next_hops = ['arrest1']
prison3_condition = ['None']
AddStep(steps, 'prison3', prison3_description, prison3_actions, prison3_next_hops, prison3_condition)

arrest1_description = 'Тучи налились свинцом. Сонный город ослепила молния.\n' \
                      'По городу прокатился мощной раскат грома.Первые капли разбились о сухую землю.\n\n' \
                      'Вся добытая Даком этой длинной ночью информация о преступнике была передана шерифу,\n' \
                      'который при помощи целого штата ищеек, в течение получаса, смог найти предполагаемое место,\n' \
                      'где Фокс залёг на дно. Возможно, это было единственное, в карьере шерифа,\n' \
                      'что ему удалось быстро выполнить. Моментально было принято решение о проведении штурма.\n\n' \
                      'Возгордившись удачно проделанной работой, шериф предложил Даку принять в этом участие.\n' \
                      'Для Дака это было делом чести, и, не сомневаясь ни секунды, Дак согласился.\n\n' \
                      'Минута… и все полицейские города оголтелой толпой мчались\n' \
                      'на заброшенную много лет мебельную фабрику.' \
                      'Единственный, кто сохранял спокойствие-детектив.\n\n' \
                      '-Ты уверен, что хочешь там быть?- поинтересовался Шериф.\n\n' \
                      'Абсолютно.-шепнул Дак. Он был не склонен менять свои решения, особенно, правильные.\n\n' \
                      'Начался штурм. Бронированные по самые уши полицейские группы захвата проникли на фабрику\n' \
                      'и начали проводить поиски Фокса. В их рядах находился и Дак.\n' \
                      'Очередь хлопков и группа захвата, славившаяся свирепостью и результативностью,\n' \
                      'опала, как пожухлая листва осеннего леса. Дак сбит с ног.\n' \
                      'Из самой тьмы перед ним возникает демон, призрак, ничто, одно имя…\n' \
                      'Вся жизнь проносится перед глазами Дака. Возможно, это последний момент в ней.\n' \
                      'Тень в полуметре от него… Принято судьбоносное решение…\n'
arrest1_actions = ['Попытаться договориться', 'Выхватить пистолет и попытаться выстрелить',
                   'Выхватить пистолет и выстрелить', 'Позвонить мистеру Вулфу']
arrest1_next_hops = ['arrest2', 'arrest3', 'arrest4', 'arrest5']
arrest1_condition = ['None', 'Пистолет', 'Пистолет с патронами', 'Номер телефона мистера Вулфа']
AddStep(steps, 'arrest1', arrest1_description, arrest1_actions, arrest1_next_hops, arrest1_condition)

arrest2_description = '-Слушай, я частный детек…\n\n' \
                      'Выстрел. Навыки парламентёра бесполезны в общении с бешеной псиной…\n\n' \
                      'Конец предсказуем. Дождь. Кладбище. Залпы почётного караула.\n' \
                      'Флаг, свёрнутый треугольником, лежащий на гробу. Барабанная дробь. Занавес.\n'
arrest2_actions = ['Выйти из игры']
arrest2_next_hops = ['final']
arrest2_condition = ['None']
AddStep(steps, 'arrest2', arrest2_description, arrest2_actions, arrest2_next_hops, arrest2_condition)

arrest3_description = 'Рука машинально потянулась за пистолетом. Щелчок.\n' \
                      'Нет патронов. Ответный выстрел. Тьма. Тишина…\n\n' \
                      'Конец предсказуем. Дождь. Кладбище. Залпы почётного караула.\n' \
                      'Флаг, свёрнутый треугольником, лежащий на гробу. Барабанная дробь. Занавес.\n'
arrest3_actions = ['Выйти из игры']
arrest3_next_hops = ['final']
arrest3_condition = ['None']
AddStep(steps, 'arrest3', arrest3_description, arrest3_actions, arrest3_next_hops, arrest3_condition)

arrest4_description = 'Рука машинально потянулась за пистолетом. Выстрел.\n' \
                      'Твёрдая рука и меткий глаз не подвели детектива.\n' \
                      'Вся стена напротив Дака окроплена малиновым вареньем. Фокс мертв. Добро торжествует.\n' \
                      'Зло повержено, только потому что оно зло. Истинные его мотивы смоются водопадами истории\n' \
                      'и канут в небытие… Очередная ночь Лучшего города…\n' \
                      'В Лучшем городе либо выживают с широко закрытым звериным оскалом, либо…не выживают…\n'
arrest4_actions = ['Выйти из игры']
arrest4_next_hops = ['final']
arrest4_condition = ['None']
AddStep(steps, 'arrest4', arrest4_description, arrest4_actions, arrest4_next_hops, arrest4_condition)

arrest5_description = '-Слушай, я частный детек…\n\n' \
                      'Выстрел. Навыки парламентёра бесполезны в общении с бешеной псиной…\n' \
                      'Если только заранее не заручиться поддержкой другой такой же.\n\n' \
                      'Лицо лежащего Дака окроплено малиновым вареньем. Фокс мертв.\n' \
                      'Мистер Вульф на славу выполнил своё дело – решил проблему.\n\n' \
                      'Добро торжествует. Зло повержено, только потому что оно зло.\n' \
                      'Истинные его мотивы смоются водопадами истории и канут в небытие…\n' \
                      'Очередная ночь Лучшего города…\n' \
                      'В Лучшем городе либо выживают с широко закрытым звериным оскалом, либо…не выживают…\n' \
                      ''
arrest5_actions = ['Выйти из игры']
arrest5_next_hops = ['final']
arrest5_condition = ['None']
AddStep(steps, 'arrest5', arrest5_description, arrest5_actions, arrest5_next_hops, arrest5_condition)


if __name__ == '__main__':
    Game(steps)

