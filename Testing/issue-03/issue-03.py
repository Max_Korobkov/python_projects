from typing import List, Tuple
import unittest


def fit_transform(*args: str) -> List[Tuple[str, List[int]]]:
    """
    fit_transform(iterable)
    fit_transform(arg1, arg2, *args)
    """
    if len(args) == 0:
        raise TypeError('expected at least 1 arguments, got 0')

    categories = args if isinstance(args[0], str) else list(args[0])
    uniq_categories = set(categories)
    bin_format = f'{{0:0{len(uniq_categories)}b}}'
    seen_categories = dict()
    transformed_rows = []

    for cat in categories:
        bin_view_cat = (int(b) for b in bin_format.format(1 << len(seen_categories)))
        seen_categories.setdefault(cat, list(bin_view_cat))
        transformed_rows.append((cat, seen_categories[cat]))

    return transformed_rows


class TestFitTransform(unittest.TestCase):
    def test_str(self):
        result = fit_transform('Moscow', 'Paris', 'London', 'Rio')
        expected = [
            ('Moscow', [0, 0, 0, 1]),
            ('Paris', [0, 0, 1, 0]),
            ('London', [0, 1, 0, 0]),
            ('Rio', [1, 0, 0, 0]),
        ]

        self.assertEqual(result, expected)


    def test_repeat(self):
        result = fit_transform('Moscow', 'London', 'Moscow', 'London')
        expected = [
            ('Moscow', [0, 1]),
            ('London', [1, 0]),
            ('Moscow', [0, 1]),
            ('London', [1, 0]),
        ]

        self.assertEqual(result, expected)


    def test_noparameter(self):
        with self.assertRaises(TypeError):
            fit_transform()


    def test_list(self):
        data = ['Moscow', 'Paris', 'London']
        result = fit_transform(data)
        expected =[
            ('Moscow', [0, 0, 1]),
            ('Paris', [0, 1, 0]),
            ('London', [1, 0, 0]),
        ]

        self.assertEqual(result, expected)


    def test_tuple(self):
        data = ('Moscow', 'Paris', 'London')
        result = fit_transform(data)
        expected = [
            ('Moscow', [0, 0, 1]),
            ('Paris', [0, 1, 0]),
            ('London', [1, 0, 0]),
        ]

        self.assertEqual(result, expected)

    def test_dict(self):
        data = {'Moscow': 1, 'Paris': 2, 'London': 3}
        result = fit_transform(data)
        expected = [
            ('Moscow', [0, 0, 1]),
            ('Paris', [0, 1, 0]),
            ('London', [1, 0, 0]),
        ]

        self.assertEqual(result, expected)

    def test_set(self):
        data = {'Moscow', 'Paris', 'London'}
        result = fit_transform(data)

        self.assertIn(result[0][0], data)



if __name__ == '__main__':
    from pprint import pprint
    unittest.main()
    cities = ['Moscow', 'New York', 'Moscow', 'London']
    exp_transformed_cities = [
        ('Moscow', [0, 0, 1]),
        ('New York', [0, 1, 0]),
        ('Moscow', [0, 0, 1]),
        ('London', [1, 0, 0]),
    ]
    transformed_cities = fit_transform(cities)
    pprint(transformed_cities)
    assert transformed_cities == exp_transformed_cities
