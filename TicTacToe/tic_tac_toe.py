
def begin() -> dict:
    """Функция, получающая данные о выбранных для игры настройках.

    Осуществляется:
    -Выбор размерности игры;
    -Выбор символов для игры.
    Возвращает словарь с введенными значениями. Если значения не введены,
    то возвращается пустой словарь.
    """
    while True:

        print("Нажми 'Enter', чтобы начать играть. Также можешь ввести размер поля от 3 до 5 (по умолчанию: 3).\n"
              "Если хочешь поменять символы, которые будут в игре, то введи 'symb' (без кавычек).\n"
              "Параметры записываются через запятую (Пример: 4,symb).")
        parameters = input().split(',')
        parameters_parsed = list()
        for parameter in parameters:
            parameters_parsed.append(parameter.strip())
        if len(parameters_parsed) == 1:
            if parameters_parsed[0] == '':
                return dict()
            elif parameters_parsed[0] == 'symb':
                return dict(symb=True)
            else:
                try:
                    pole_size = int(parameters_parsed[0])
                    if pole_size < 3 or pole_size > 5:
                        raise ValueError
                except ValueError:
                    print('Неверно введен параметр. Попробуй снова.')
                else:
                    return dict(pole_size=pole_size)
        elif len(parameters_parsed) == 2:
            if parameters_parsed[0] == 'symb':
                try:
                    pole_size = int(parameters_parsed[1])
                    if pole_size < 3 or pole_size > 5:
                        raise ValueError
                except ValueError:
                    print('Неверно введен размер. Попробуй снова.')
                else:
                    return dict(pole_size=pole_size, symb=True)
            elif parameters_parsed[1] == 'symb':
                try:
                    pole_size = int(parameters_parsed[0])
                except ValueError:
                    print('Неверно введен размер. Попробуй снова.')
                else:
                    return dict(pole_size=pole_size, symb=True)
            else:
                print('Неверно введены параметры. Попробуй снова.')
        else:
            print('Неверно введены параметры. Попробуй снова.')


def init(**parameters) -> dict:
    """Функция, инициализирующая настройки.

    Осуществляется:
    -Определение размера поля (int);
    -Определение символов для игры (list);
    -Определение множеств ходов каждого из игроков (set(typle)).
    Возвращает словарь с введенными значениями.
    """
    parameters.setdefault('pole_size', 3)
    parameters.setdefault('symb', False)
    player_symbols = []
    if parameters.get('symb') is True:
        player_symbols = enter_symbol(1, player_symbols)
        player_symbols = enter_symbol(2, player_symbols)
    else:
        player_symbols = ['X', '0']
    parameters.setdefault('player_symbols', player_symbols)
    parameters.setdefault('first_player_moves', set())
    parameters.setdefault('second_player_moves', set())
    return parameters


def enter_symbol(player_number: int, player_symbols: list):
    """Функция, определяющая символ для игрока.

    На входе ожидается:
    -Номер игрока (int);
    -Выбранные символы игроков (list(str)).
    Возвращает список с добавленным символом.
    """
    player_symbols_set = set(player_symbol for player_symbol in player_symbols)
    while True:
        print(f'{player_number}-й игрок, введи символ, которым будешь играть.\n'
              'В случае ввода последовательности все символы кроме 1-го будут отброшены,\n'
              'а буква будет приведена к верхнему регистру.')
        enter_symbol = input().strip()
        if enter_symbol == '' or enter_symbol.isspace():
            print('Забавно играть пробелами. Попробуй что-нибудь другое.')
            continue
        elif {enter_symbol[0]}.issubset(player_symbols_set):
            print('В фантазии тебе не занимать. Попробуй что-нибудь другое.')
            continue
        elif {enter_symbol[0].upper()}.issubset(player_symbols_set):
            print('Сменить регистр не получится. Попробуй что-нибудь другое.')
            continue
        else:
            player_symbols.append(enter_symbol[0].upper())
            return player_symbols


def win_combinations_generator(parameters: dict) -> set:
    """Функция, определяющая множество выигрышных комбинаций для игры.

    На входе ожидается:
    -Заполненный словарь parameters;
    Возвращает множество с выигрышными комбинациями (set(frozenset(tuple))).
    """
    win_comb = set()
    win_comb.update({frozenset((vert_idx, hor_idx) for hor_idx in range(parameters['pole_size']))
                     for vert_idx in range(parameters['pole_size'])},
                    {frozenset((vert_idx, hor_idx) for vert_idx in range(parameters['pole_size']))
                     for hor_idx in range(parameters['pole_size'])},
                    {frozenset((idx, idx) for idx in range(parameters['pole_size']))},
                    {frozenset((idx, parameters['pole_size']-1-idx) for idx in range(parameters['pole_size']))})
    return win_comb


def step(player_number: int, parameters: dict) -> dict:
    """Функция, добавляющая шаг в множество шагов игрока.

        На входе ожидается:
        -Номер игрока;
        -Заполненный словарь parameters.
        Возвращает словарь с шагом игрока.
        """
    print(f'{player_number}-й игрок, введи клетку, в которую хочешь пойти.\n'
          f'Пример (A1)')
    while True:
        player_input = input().strip().upper()
        if len(player_input) != 2:
            print('Неверный ввод (больше символов в строке).')
            continue
        else:
            if 48 < ord(player_input[0]) < 48+1+parameters['pole_size']:
                if 64 < ord(player_input[1]) < 64+1+parameters['pole_size']:
                    now_step = (ord(player_input[1])-65, ord(player_input[0])-49)
                    if {now_step}.issubset(parameters['first_player_moves']) or\
                            {now_step}.issubset(parameters['second_player_moves']):
                        print('Такой ход уже был.')
                        continue
                    else:
                        if player_number == 1:
                            parameters['first_player_moves'].add(now_step)
                        else:
                            parameters['second_player_moves'].add(now_step)
                        return parameters
                else:
                    print('Неверный ввод (ошибка в букве).')
                    continue
            elif 64 < ord(player_input[0]) < 64+1+parameters['pole_size']:
                if 48 < ord(player_input[1]) < 48+1+parameters['pole_size']:
                    now_step = (ord(player_input[0]) - 65, ord(player_input[1]) - 49)
                    if {now_step}.issubset(parameters['first_player_moves']) or\
                            {now_step}.issubset(parameters['second_player_moves']):
                        print('Такой ход уже был.')
                        continue
                    else:
                        if player_number == 1:
                            parameters['first_player_moves'].add(now_step)
                        else:
                            parameters['second_player_moves'].add(now_step)
                        return parameters
                else:
                    print('Неверный ввод (ошибка в числе).')
                    continue
            else:
                print('Неверно введена команда.')
                continue


def pole_show(parameters: dict) -> None:
    """Функция, рисующая игровое поле на экране.

        На входе ожидается:
        -Заполненный словарь parameters.
        """
    delimiter = '-'*(4*(parameters['pole_size']+1))
    print('r-c', end='')
    for vert_idx in range(parameters['pole_size']):
        print(f'| {chr(vert_idx+65)} ', end='')
    print('|')
    for hor_idx in range(parameters['pole_size']):
        print(delimiter)
        print(f' {hor_idx+1} ', end='')
        for vert_idx in range(parameters['pole_size']):
            print('|', end='')
            if {(vert_idx, hor_idx)}.issubset(parameters['first_player_moves']):
                print(f" {parameters['player_symbols'][0]} ", end='')
            elif {(vert_idx, hor_idx)}.issubset(parameters['second_player_moves']):
                print(f" {parameters['player_symbols'][1]} ", end='')
            else:
                print('   ', end='')
        print('|')
    print(delimiter)


def win_compare(parameters: dict, win_combinations: set) -> bool:
    """Функция, определяющая победу.

        На входе ожидается:
        -Заполненный словарь parameters;
        -Множество выигрышных комбинаций.
        Возвращает флаг наличия выигрыша.
        """
    for win_combination in win_combinations:
        if win_combination.issubset(parameters['first_player_moves']) or\
                win_combination.issubset(parameters['second_player_moves']):
            return True
        else:
            continue
    return False


def game():
    """Функция с игрой."""
    parameters = begin()
    parameters = init(**parameters)
    win_combinations = win_combinations_generator(parameters)
    while True:
        for player_number in range(2):
            parameters = step(player_number+1, parameters)
            pole_show(parameters)
            if win_compare(parameters, win_combinations):
                print(f'Победил {player_number+1}-й игрок !!!')
                return


if __name__ == '__main__':
    game()
